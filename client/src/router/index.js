import MovieItemPage from '@/pages/MovieItemPage.vue';
import MoviesPage from '@/pages/MoviesPage.vue';
import { createRouter, createWebHistory } from 'vue-router';

const routes = [
  { path: '/', component: MoviesPage },
  { path: '/movie/:id', component: MovieItemPage },
];

export const router = createRouter({
  history: createWebHistory(),
  routes,
});
