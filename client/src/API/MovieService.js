import axios from 'axios';

export class MovieService {
  static url = 'http://localhost:9000/api';

  static async getAll() {
    const response = await axios.get(`${this.url}/movies`);

    return response.data.data;
  }

  static async getMovie(id) {
    const response = await axios.get(`${this.url}/movie/${id}`);

    return response.data.data;
  }
}
