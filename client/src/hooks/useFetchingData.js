import { onMounted, ref } from 'vue';

export const useFetchingData = (Service) => {
  const data = ref([]);
  const isLoading = ref(true);

  const fetchData = async () => {
    try {
      const response = await Service;

      if (Array.isArray(response)) {
        return (data.value = [...data.value, ...response]);
      }

      if (response) {
        return (data.value = { ...data.value, ...response });
      }
    } catch (e) {
      throw new Error(e.message);
    } finally {
      isLoading.value = false;
    }
  };

  onMounted(fetchData);

  return {
    data,
    isLoading,
  };
};
