import { ref, computed } from 'vue';

export const useSortedMovies = (movies) => {
  const selectedSort = ref('');
  const sortedPost = computed(() => {
    switch (selectedSort.value) {
      case 'title':
        return [...movies.value].sort((movie1, movie2) =>
          movie1[selectedSort.value]?.localeCompare(movie2[selectedSort.value])
        );
      case 'year':
        return [...movies.value].sort(
          (movie1, movie2) =>
            movie1[selectedSort.value] - movie2[selectedSort.value]
        );
      default:
        return [...movies.value];
    }
  });

  return {
    selectedSort,
    sortedPost,
  };
};
